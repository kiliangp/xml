<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html" indent="yes"/>
   <xsl:template match="/" >
      
       <html>
           <head>
               <title>LOGO</title>
               <meta charset="UTF-8" />
               <link rel="stylesheet" href="style.css" />
           </head>
          
           <body>
                <table>
                    <thead>
                        <tr class="violeta">
                            <td>Logo</td>
                            <td>Name</td>
                            <td>Type</td>
                            <td>License</td>
                        </tr>
                    </thead>

                    <tbody>
                        <xsl:for-each select="//program">
                            <tr>
                                <td>
                                    <xsl:variable name="logo_img" select="logo" />
                                        <img src="{$logo_img}" />
                                </td>

                                <td>
                                    <xsl:value-of select="name" />
                                </td>

                                <td>
                                    <xsl:value-of select="type" />
                                </td>

                                <td>
                                    <xsl:value-of select="developers" />
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
           </body>
       </html>

   </xsl:template>
</xsl:stylesheet>