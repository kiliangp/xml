<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
        
        <html>
            <head>
                <title>FILM LIST</title>
                <meta charset="UTF-8" />
                <link rel="stylesheet" href="style.css" />
            </head>
            
            <body>
                <h2>Film List</h2>
                
                <table style="border: 1px solid black">
                    <thead>
                        <tr bgcolor="blue">
                            <td>Title</td>
                            <td>Language</td>
                            <td>Year</td>
                            <td>Country</td>
                            <td>Genre</td>
                            <td>Summary</td>
                        </tr>
                    </thead>

                    <tbody>
                        <xsl:for-each select="films/film">
                            <tr>
                                <td>
                                    <xsl:value-of select="title" />
                                </td>

                                <td>
                                    <xsl:value-of select="title/@lang" />
                                </td>

                                <td>
                                    <xsl:value-of select="year" />
                                </td>

                                <td>
                                    <xsl:value-of select="country" />
                                </td>

                                <td>
                                    <xsl:value-of select="genre" />
                                </td>

                                <td>
                                    <xsl:value-of select="summary" />
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </body>
        </html>    
    </xsl:template>
</xsl:stylesheet>