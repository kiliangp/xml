<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
        <html>
            <head>
            <meta charset="UTF-8"/>
            <link rel="stylesheet" href="style.css" />
            </head>
            <body>
            <h2>Bookstore</h2>
                <xsl:for-each select="bookstore/book">
                 <xsl:sort select="@category" />
                    <table>
                        <tr>
                            <td class="columna1">Title (lang: <xsl:value-of select="title/@lang"/>)</td>
                            <td><xsl:value-of select="title" /></td>
                        </tr>

                        <tr>
                            <td class="columna1">Category</td>
                            <td><xsl:value-of select="@category" /></td>
                        </tr>
                        <tr>
                            <td class="columna1">Year</td>
                            <td><xsl:value-of select="year" /></td>
                        </tr>

                        <tr>
                            <td class="columna1">Price</td>
                            <td>
                                <xsl:value-of select="price" />
                                 <xsl:text> €</xsl:text>
                            </td>
                        </tr>

                        <tr>
                            <td class="columna1">Format</td>
                            <td><xsl:value-of select="format/@type" /></td>
                        </tr>

                        <tr>
                            <td class="columna1">ISBN</td>
                            <td><xsl:value-of select="isbn" /></td>
                        </tr>

                        <tr class="autors">
                            <td colspan="2">Authors:</td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <xsl:for-each select="author">
                                    <xsl:sort select="." />
                                    <xsl:value-of select="." />
                                    <xsl:if test="position() != last()"><br /></xsl:if>
                                </xsl:for-each>
                            </td>
                        </tr>

                        <br/>
                    </table>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>