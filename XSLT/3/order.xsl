<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html" indent="yes"/>
   <xsl:template match="/" >
      
       <html>
           <head>
               <title>LOGO</title>
               <meta charset="UTF-8" />
               <link rel="stylesheet" href="style.css" />
           </head>
          
           <body>
                <h2>Products with a Price &gt; 25 a Price &lt;= 100</h2>
                <br/>
                <br/>


                <table class="table1">
                    <thead class="thead1">
                        <th colspan="2" bgcolor="blue" style="padding:15px; font-weight: bold; font-size: 18px">
                            CUSTOMER DETAILS
                        </th>
                    </thead>

                    <tbody>
                        <xsl:for-each select="order/destination">
                            <tr class="tr1">
                                <td class="td1">Name</td>
                                <td class="td11">
                                    <xsl:value-of select="name"/> 
                                </td>
                            </tr>

                            <tr class="tr1">
                                <td class="td1">Address</td>
                                <td class="td1">
                                    <xsl:value-of select="address"/>
                                </td>
                            </tr>

                            <tr class="tr1">
                                <td class="td1">City</td>
                                <td class="td1">
                                    <xsl:value-of select="city"/>
                                </td>
                            </tr>

                            <tr class="tr1">
                                <td class="td1">P.C</td>
                                <td class="td1">
                                    <xsl:value-of select="postalcode"/>
                                </td>
                            </tr>

                        </xsl:for-each>
                    </tbody>
                </table>

                <br/>
                <br/>
                <br/>

                <table class="table2">
                    <thead class="thead2">
                        <th colspan="4" bgcolor="blue" style="padding:10px; font-size:19px">ORDER</th>
                    </thead>

                    <tbody>
                            <tr>
                                <td class="td2">Product</td>
                                <td class="td2">Price</td>
                                <td class="td2">Quantity</td>
                                <td class="td2">Total</td>
                            </tr>

                            <xsl:for-each select="order/products/product">
                            <xsl:sort select="name"/>
                                <xsl:if test="price > 25 and price <= 100">
                                    <tr>
                                        <xsl:choose>
                                            <xsl:when test="price > 25 and price < 50">
                                                <td bgcolor="yellow"><xsl:value-of select="name"/> (code = <xsl:value-of select="@code"/>)</td>
                                                <td bgcolor="yellow"><xsl:value-of select="price"/></td>
                                                <td bgcolor="yellow"><xsl:value-of select="quantity"/></td>
                                                <td bgcolor="yellow"><xsl:value-of select="price * quantity"/></td>
                                            </xsl:when>
                                            <xsl:when test="price >= 50 and price < 75">
                                                <td bgcolor="green"><xsl:value-of select="name"/> (code = <xsl:value-of select="@code"/>)</td>
                                                <td bgcolor="green"><xsl:value-of select="price"/></td>
                                                <td bgcolor="green"><xsl:value-of select="quantity"/></td>
                                                <td bgcolor="green"><xsl:value-of select="price * quantity"/></td>
                                            </xsl:when>
                                            <xsl:when test="price >= 75 and price < 100">
                                                <td bgcolor="red"><xsl:value-of select="name"/> (code = <xsl:value-of select="@code"/>)</td>
                                                <td bgcolor="red"><xsl:value-of select="price"/></td>
                                                <td bgcolor="red"><xsl:value-of select="quantity"/></td>
                                                <td bgcolor="red"><xsl:value-of select="price * quantity"/></td>
                                            </xsl:when>
                                        </xsl:choose>
                                    </tr>
                                </xsl:if>

                            </xsl:for-each>
                    </tbody>
                </table>

           </body>
        
        </html>
    </xsl:template>
</xsl:stylesheet>