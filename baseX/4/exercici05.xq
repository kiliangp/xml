let $llibres := doc("/home/kilian/HTML/baseX/library.xml")/library/book

for $llistat in $llibres
where $llistat/@year > "1992" and $llistat/editorial="Addison-Wesley"
return ($llistat/title, data($llistat/@year))