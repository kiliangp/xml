let $llibres := doc("/home/kilian/HTML/baseX/library.xml")/library/book

for $count_autors in $llibres
where count($count_autors/author < 1 )
return ($count_autors/title, data($count_autors/@year))