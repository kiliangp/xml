let $llibres_any := doc("/home/users/inf/hisx2/a221587kg/2ASIX/LLENGUATGE/UF2/EJERCICIOS/baseX/library.xml")/library/book

for $ordenat in $llibres_any
order by $ordenat/@year
return data(($ordenat/title,$ordenat/@year))
