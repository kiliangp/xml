let $llibres := distinct-values(doc("/home/kilian/HTML/baseX/library.xml")/library/book/author/lastname)


for $cognom in $llibres
order by $cognom
return ($cognom)
