let $doc := doc("/home/users/inf/hisx2/a221587kg/2ASIX/LLENGUATGE/UF2/EJERCICIOS/baseX/institut.xml")
return
<institut>

{
  for $i in $doc/institut/alumnes/alumne
  where $i/edat>20
  order by $i/nom descending
  return <alumne>{$i/* }</alumne>
}
</institut>
(:ENGLOBAMOS TODO EL RESULTADO Y LE PONEMOS UNA NUEVA ETIQUETA "<institut>":)