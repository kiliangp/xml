for $preus in doc("/home/kilian/HTML/baseX/auto.xml")/autos/vehicles/vehicle[price > 20000]

order by $preus/price ascending

return concat(data($preus/model), " - ", data($preus/price))