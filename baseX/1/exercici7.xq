for $models in doc("/home/kilian/HTML/baseX/auto.xml")/autos/vehicles/vehicle

where $models/year > 2007 
order by $models/year descending
return ($models/model, $models/year)