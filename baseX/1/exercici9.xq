for $preus in doc("/home/kilian/HTML/baseX/auto.xml")/autos/vehicles/vehicle

order by $preus/price ascending
return (data($preus/model), data($preus/price))