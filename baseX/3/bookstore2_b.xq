<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Bookstore List</title>
    <link rel="stylesheet" href="style2_b.css" />
</head>
<body>
  <table>
    <tr>
      <th colspan="5" class="nombre" style="text-align:left">
        {
          let $nombre := doc("/home/users/inf/hisx2/a221587kg/2ASIX/LLENGUATGE/UF2/EJERCICIOS/baseX/bookstore.xml")/bookstore/book[title="TCP/IP Illustrated"]/author[lastname="Stevens"]/lastname
          return data($nombre) 
      } books
       </th>
    </tr>
    <tr style="background-color: cornflowerblue;">
      <td>TITLE</td>
      <td>YEAR</td>
      <td>AUTHORS</td>
      <td>EDITORIAL</td>
      <td>PRICE</td>
    </tr>
    
    {
      for $books in doc("/home/users/inf/hisx2/a221587kg/2ASIX/LLENGUATGE/UF2/EJERCICIOS/baseX/bookstore.xml")/bookstore/book
      where $books/author/lastname="Stevens"
      return
        <tr>
          <td>
            {data($books/title)}
          </td>
          
          <td>
            {data($books/@year)}
          </td>
          
          <td>
            {
              for $autor in $books/author
              return
                <p>{(concat(data($autor/name), " ", data($autor/lastname)))}
                </p>}
          
          </td>
          
          <td>
            {$books/editorial}
          </td>
          
          <td>
            {$books/price}
          </td>
        </tr>  
   
}
    <tfoot>
          {
            let $preus_llibres := doc("/home/users/inf/hisx2/a221587kg/2ASIX/LLENGUATGE/UF2/EJERCICIOS/baseX/bookstore.xml")/bookstore/book[author/lastname = "Stevens"]
            return 
        <tr>
          <td colspan="4" style="font-weight: bold; text-align: right">Total price</td>
          <td>
             {data(sum($preus_llibres/price))} 
          </td>
      
      </tr>
    }
    </tfoot>  
  </table>
</body>

</html>