<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Bookstore List</title>
    <link rel="stylesheet" href="style2.css" />
</head>
<body>
  <table>
    <tr>
      <th colspan="5" class="nombre" style="text-align: left">
      {
        let $nom := doc("/home/users/inf/hisx2/a221587kg/2ASIX/LLENGUATGE/UF2/EJERCICIOS/baseX/bookstore.xml")/bookstore/book[title="Data on the Web"]/author[lastname="Abiteboul" and name="Serge"]/lastname
        return data($nom)
      } books
      </th>
    </tr>
    
    <tr class="cabecero">
      <td>TITLE</td>
      <td>YEAR</td>
      <td>AUTHOR</td>
      <td>EDITORIAL</td>
      <td>PRICE</td>
    </tr>
    
    <tbody>
    {
      for $llibre in doc("/home/users/inf/hisx2/a221587kg/2ASIX/LLENGUATGE/UF2/EJERCICIOS/baseX/bookstore.xml")/bookstore/book
      where $llibre/author/lastname="Abiteboul"
      order by $llibre
      return 
        <tr>
          <td>
            {data($llibre/title)}
          </td>
          
          <td>
            {data($llibre/@year)}
          </td>
          
          <td>
            {
              for $autor in $llibre/author
              return
                <p>{(concat(data($autor/name), " ", data($autor/lastname)))}
                </p>}
          
          </td>
          <td>
            {$llibre/editorial}
          </td>
          
          <td>
            {$llibre/price}
          </td>
        </tr>
        
}
    </tbody>
    
    <tfoot>
            { 
            let $preus_llibres := doc("/home/users/inf/hisx2/a221587kg/2ASIX/LLENGUATGE/UF2/EJERCICIOS/baseX/bookstore.xml")/bookstore/book[author/lastname = "Abiteboul"]
    return
          
      <tr>
        <td colspan="4" style="text-align:right; font-weight: bold">Total price</td>
        <td>{data(sum($preus_llibres/price))}</td>
      </tr>
    }
    </tfoot>
  </table>
</body>
</html>