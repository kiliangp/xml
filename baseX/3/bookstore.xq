<html lang="en">
<head>
  <meta charset="utf-8"/>
  <title>Book List</title>
  <link rel="stylesheet" href="style.css" />
</head>

<body>
  <table>
    <tr style="text-align: center">
      <th>TITLE</th>    
      <th>EDITORIAL</th>
      <th>PRICE</th>
    </tr>
    {
    for $book in doc("/home/users/inf/hisx2/a221587kg/2ASIX/LLENGUATGE/UF2/EJERCICIOS/baseX/bookstore.xml")/bookstore/book
    order by $book
    return
    <tr>
      <td>
         {data($book/title)}
      </td>
      
      <td>
        {$book/editorial}
      </td>
      
      <td>
        {$book/price}
      </td>
    </tr>
  }
  </table>
</body>
</html>
