declare function local:expenses($preu as xs:decimal?) as xs:decimal?

{
    let $expen := ($preu * 0.21) + 2.5
    let $taxa := 2.5
    return round(($expen + $taxa),2)
};

for $games in /playstore/games/game

return 
<product>
{
  ($games/name, $games/price)

}

{<expenses>
{local:expenses($games/price)}
</expenses>}

</product>

